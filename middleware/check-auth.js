/* eslint-disable consistent-return */
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  // console.log(req.headers.authorization.split(' '))
  try {
    jwt.verify(req.headers.authorization.split(' ')[1], process.env.SECRET_KEY);

    next();
  } catch (error) {
    return res.status(401).json({ message: 'Auth failed' });
  }
};
