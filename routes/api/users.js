/* eslint-disable no-console */
const express = require('express');

const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const users = require('../../db/userDb');
require('dotenv').config();
const validation = require('../../validation/index');


router.post('/signup', validation.validationUser, (req, res) => {
  const hash = bcrypt.hashSync(req.body.password, 10);
  const user = {
    username: req.body.username,
    password: hash,
  };
  users.signup(user).then((results) => {
    res.send({ results });
  }).catch((error) => {
    res.send(error);
  });
});

router.post('/token', validation.validationUser, (req, res) => {
  const userName = req.body.username;
  const passWord = req.body.password;
  if (userName && passWord) {
    users.userLogin(userName).then((result) => {
      if (bcrypt.compareSync(passWord, result[0].password)) {
        const token = jwt.sign({ result }, process.env.SECRET_KEY, {
          expiresIn: 1440,
        });
        res.send({ token, userName });
      } else {
        res.send('Incorrect Username and/or Password!');
      }
      res.end();
    }).catch((err) => {
      console.log(err);
    });
  } else {
    res.send('Please enter Username and Password!');
    res.end();
  }
});

module.exports = router;
