/* eslint-disable consistent-return */
const express = require('express');

const router = express.Router();
const createError = require('http-errors');
const dbFunctions = require('../../db/songsDb');

const { getAllSongs } = dbFunctions;
const { getSongById } = dbFunctions;
const { deleteSongById } = dbFunctions;
const { addSong } = dbFunctions;
const { updateSong } = dbFunctions;
const validation = require('../../validation/index');
const checkAuth = require('../../middleware/check-auth');


// Getting all songs

router.get('/', (req, res) => {
  getAllSongs()
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

// Getting songs by ID

router.get('/:id', async (req, res, next) => {
  const songId = Number(req.params.id);
  const result = await getSongById(songId);
  if (result.length === 0) {
    return next(createError(400, 'Invalid id'));
  }
  getSongById(songId)
    .then(() => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

// Delete songs by Id

router.delete('/:id', checkAuth, async (req, res, next) => {
  const songId = Number(req.params.id);
  const result = await deleteSongById(songId);
  if (result.affectedRows === 0) {
    return next(createError(400, 'Invalid id'));
  }
  deleteSongById(songId)
    .then(() => {
      res.json({ message: 'deletion successful' });
    })
    .catch((err) => {
      res.send(err);
    });
});

// Add song

router.post('/', checkAuth, validation.validationSong, (req, res) => {
  const newSong = req.body;
  addSong(newSong)
    .then(() => {
      res.json({ message: 'adding successful' });
    })
    .catch((err) => {
      res.send(err);
    });
});

// Update song

router.put(
  '/:id',
  checkAuth,
  validation.validationSong,
  async (req, res, next) => {
    const newSongId = req.params.id;
    const newSongBody = req.body;
    const result = await updateSong(newSongId, newSongBody);
    if (result.affectedRows === 0) {
      return next(createError(400, 'Invalid id'));
    }
    updateSong(newSongId, newSongBody)
      .then(() => {
        res.json({ message: 'updation successful' });
      })
      .catch((err) => res.send(err));
  },
);

module.exports = router;
