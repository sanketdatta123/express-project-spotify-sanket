const express = require('express');
const createError = require('http-errors');

const router = express.Router();
const dbFunctions = require('../../db/Artistsdb');

const { getAllArtists } = dbFunctions;
const getAllArtistsById = dbFunctions.getArtistById;
const { deleteArtistById } = dbFunctions;
const { addArtist } = dbFunctions;
const { updateArtist } = dbFunctions;
const validation = require('../../validation/index');
const checkAuth = require('../../middleware/check-auth');

// Getting all artists

router.get('/', (req, res) => {
  getAllArtists()
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

// Getting artist by id

router.get('/:id', async (req, res, next) => {
  const artistId = Number(req.params.id);
  const result = await getAllArtistsById(artistId);
  if (result.length === 0) {
    return next(createError(400, 'Invalid id'));
  }
  return getAllArtistsById(artistId)
    .then((artistList) => {
      res.json(artistList);
    })
    .catch((err) => {
      res.send(err);
    });
});

// Deleting artist

router.delete('/:id', checkAuth, async (req, res, next) => {
  const artistId = Number(req.params.id);
  const result = await deleteArtistById(artistId);
  if (result.affectedRows === 0) {
    return next(createError(400, 'Invalid id'));
  }
  return deleteArtistById(artistId)
    .then(() => {
      res.json({ message: 'deleting successful' });
    })
    .catch((err) => {
      res.send(err);
    });
});

// Adding artist

router.post(
  '/',
  checkAuth,
  validation.validationArtist,
  async (req, res, next) => {
    const newArtist = req.body;
    addArtist(newArtist)
      .then(() => {
        res.json({ message: 'adding successful' });
      })
      .catch(() => next(createError(400, 'Invalid inputt')));
  },
);

// Updating Artist

router.put(
  '/:id',
  checkAuth,
  validation.validationArtist,
  async (req, res, next) => {
    const newArtistId = req.params.id;
    const newArtistBody = req.body;
    const result = await updateArtist(newArtistBody, newArtistId);
    if (result.affectedRows === 0) {
      return next(createError(400, 'Invalid id'));
    }
    return updateArtist(newArtistBody, newArtistId)
      .then(() => {
        res.json({ message: 'updation successful' });
      })
      .catch((err) => {
        res.send(err);
      });
  },
);


module.exports = router;
