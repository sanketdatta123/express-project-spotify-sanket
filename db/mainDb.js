/* eslint-disable no-console */
const mysql = require('mysql');
require('dotenv').config();
// local mysql db connection
const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: 'songsql',
});

connection.connect((err) => {
  if (err) throw err;
  else {
    console.log('database connected.....');
  }
});

module.exports = connection;
