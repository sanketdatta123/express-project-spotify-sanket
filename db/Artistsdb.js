const connection = require("./mainDb");
const getAllArtistsQuery = "SELECT * FROM artists";
const getArtistByIdQuery = `SELECT * FROM artists WHERE id=?`;
const deleteArtistByIdQuery = `DELETE FROM artists WHERE id=?`;
const addArtistQuery = `INSERT INTO artists SET ?`;
const updateArtistQuery = `UPDATE artists SET ? WHERE id= ?`;


function executeSql(Query, artistId, name) {
  return new Promise((resolve, reject) => {
    connection.query(Query, [artistId, name], (err, result) => {
      if (err) reject(err);   
      else {
        resolve(result);
      }
    });
  });
}

function getAllArtists() {
  return executeSql(getAllArtistsQuery);
}

function getArtistById(artistId) {
  return executeSql(getArtistByIdQuery, artistId);
}

function deleteArtistById(artistId) {
  return executeSql(deleteArtistByIdQuery, artistId);
}

function addArtist(name) {
  return executeSql(addArtistQuery, name, null);
}

function updateArtist(Id, name) {
  return executeSql(updateArtistQuery, Id, name);
}



module.exports = {
  getAllArtists,
  getArtistById,
  deleteArtistById,
  addArtist,
  updateArtist
};
