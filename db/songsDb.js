const connection = require('./mainDb');

const getAllSongsQuery = 'SELECT * FROM songs';
const getSongByIdQuery = 'SELECT * FROM songs WHERE id= ?';
const deleteSongByIdQuery = 'DELETE FROM songs WHERE id= ?';
const addSongQuery = 'INSERT INTO songs SET ?';
const updateSongQuery = 'UPDATE songs SET ? WHERE id= ?';


function executeSql(Query, songId, songBody) {
  return new Promise((resolve, reject) => {
    connection.query(Query, [songId, songBody], (err, result) => {
      if (err) reject(err);
      else {
        resolve(result);
      }
    });
  });
}

function getAllSongs() {
  return executeSql(getAllSongsQuery);
}
function getSongById(songId) {
  return executeSql(getSongByIdQuery, songId);
}

function deleteSongById(songId) {
  return executeSql(deleteSongByIdQuery, songId);
}

function addSong(newSongBody) {
  return executeSql(addSongQuery, newSongBody);
}

function updateSong(newSongId, newSongBody) {
  return executeSql(updateSongQuery, newSongBody, newSongId);
}


module.exports = {
  getAllSongs,
  getSongById,
  deleteSongById,
  addSong,
  updateSong,
};
