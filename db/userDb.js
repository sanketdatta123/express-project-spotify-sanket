const userLoginQuery = 'SELECT password FROM accounts WHERE username = ? ';
const signUpQuery = 'INSERT INTO accounts SET ?';
const connection = require('./mainDb');


function executeSql(Query, artistId, name) {
  return new Promise((resolve, reject) => {
    connection.query(Query, [artistId, name], (err, result) => {
      if (err) reject(err);
      else {
        resolve(result);
      }
    });
  });
}


function userLogin(username) {
  return executeSql(userLoginQuery, username);
}

function signup(userData) {
  return executeSql(signUpQuery, userData);
}

module.exports = { userLogin, signup };
