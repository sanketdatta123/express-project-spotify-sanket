/* eslint-disable consistent-return */
const Joi = require('joi');
const createError = require('http-errors');


function validationSong(req, res, next) {
  const schema = Joi.object().keys({
    artist_id: Joi.number().integer().min(1),
    title: Joi.string()
      .min(3)
      .max(20),
    duration: Joi.string()
      .min(3)
      .max(5),
  });
  const result = schema.validate(req.body);
  if (result.error) {
    return next(createError(400, 'Invalid id'));
  }
  next();
}

function validationArtist(req, res, next) {
  const schema = Joi.string().required().min(4);
  const result = schema.validate(req.body.name);
  if (result.error) {
    return next(createError(400, 'Invalid input'));
  }
  next();
}

function validationUser(req, res, next) {
  const schema = Joi.object().keys({
    username: Joi.string()
      .min(3)
      .max(16),
    password: Joi.string()
      .min(5)
      .max(12),
  });
  const result = schema.validate(req.body);
  if (result.error) {
    return next(createError(400, 'Invalid input'));
  }
  next();
}

module.exports = { validationArtist, validationSong, validationUser };
