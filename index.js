/* eslint-disable no-console */
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./middleware/logger');

const app = express();

app.use(logger);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


const userRoutes = require('./routes/api/users');

app.use('/api/artists', require('./routes/api/artist'));

app.use('/api/songs', require('./routes/api/song'));

app.use('/users', userRoutes);


app.use((req, res, next) => {
  const error = new Error('Request NOt found');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

const
  port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log('server started on port 3000');
});
